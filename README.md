Capitals Weather App

This Python application retrieves the names of all the world capitals using the RestCountries API, and uses the OpenWeatherAPI to retrieve weather information for each capital. The app then processes this information and reports on the capital with the highest and lowest:

- Temperature
- Feels Like temperature
- Air pressure
- Humidity
- Wind speed
- The application uses multi-threading to retrieve weather information for multiple capitals simultaneously, making it more efficient and faster.

Installation

1. Clone the repository:

git clone https://github.com/andrzej_stopka/CapitalsWeather.git

2. Install the required Python packages:

pip install -r requirements.txt

3. Create a file called config.ini in the root directory of the project, and add your OpenWeatherAPI API key:

[openweatherapi]
api_key = YOUR_API_KEY

Usage

To run the application, simply execute the main.py file:

python main.py

The application will retrieve the weather information for all the world capitals, process the data, and display the capital with the highest and lowest values for some weather parameter.

License

This project is licensed under the MIT License. See the LICENSE file for more information.

Contributing

Contributions are welcome! If you'd like to contribute to this project, please open an issue or a pull request.
