import configparser
from concurrent.futures import ThreadPoolExecutor
import time
from utilities.city_and_api_classes import City, API
from utilities.calculations import Calculations

start = time.perf_counter()
config = configparser.ConfigParser()
config.read(r"C:\Users\ANDRZ\Desktop\Programming\Capitals_Weather\config.ini")
api_key = config.get("openweatherapi", "api_key")

api = API(api_key)

capitals = City.get_all_capitals()

with ThreadPoolExecutor(max_workers=16) as executor:
    executor.map(api.create_city_with_weather_info, capitals)

temperatures = Calculations.create_list_of_pairs("temperature", City.capital_objects)
feels_like_temperatures = Calculations.create_list_of_pairs("feels_like_temperature", City.capital_objects)
pressures = Calculations.create_list_of_pairs("pressure", City.capital_objects)
humidity = Calculations.create_list_of_pairs("humidity", City.capital_objects)
wind_speeds = Calculations.create_list_of_pairs("wind_speed", City.capital_objects)

####
# variables info: (_ _ _ )
# h - highest / l - lowest
# t - temperature / f - feels_like_temperature / p - pressure / h - humidity / w - wind_speed
# c - city / o - condition

htc, hto = max(temperatures, key=Calculations.get_condition)
ltc, lto = min(temperatures, key=Calculations.get_condition)
hfc, hfo = max(feels_like_temperatures, key=Calculations.get_condition)
lfc, lfo = min(feels_like_temperatures, key=Calculations.get_condition)
hpc, hpo = max(pressures, key=Calculations.get_condition)
lpc, lpo = min(pressures, key=Calculations.get_condition)
hhc, hho = max(humidity, key=Calculations.get_condition)
lhc, lho = min(humidity, key=Calculations.get_condition)
hwc, hwo = max(wind_speeds, key=Calculations.get_condition)
lwc, lwo = min(wind_speeds, key=Calculations.get_condition)

print(f"The highest temperature is in {htc}: {hto}°C")
print(f"The lowest temperature is in {ltc}: {lto}°C")
print(f"The highest feels like temperature is in {hfc}: {hfo}°C")
print(f"The lowest feels like temperature is in {lfc}: {lfo}°C")
print(f"The highest pressure is in {hpc}: {hpo}hPa")
print(f"The lowest pressure is in {lpc}: {lpo}hPa")
print(f"The highest humidity is in {hhc}: {hho}%")
print(f"The lowest humidity is in {lhc}: {lho}%")
print(f"The highest wind speed is in {hwc}: {hwo}m/s")
print(f"The lowest wind speed is in {lwc}: {lwo}m/s")

finish = time.perf_counter()
print(f"It took {finish - start}")
