import requests


class City:
    capital_objects = []

    @classmethod
    def get_all_capitals(cls):
        response = requests.get("https://restcountries.com/v3.1/all")
        data = response.json()
        capitals = [country["capital"][0] for country in data if "capital" in country and country["capital"][0]]
        not_found = ["St. Peter Port", "Diego Garcia", "Fakaofo", "Papeetē", "Ngerulmud", "King Edward Point"]
        capitals = list(filter(lambda city: city not in not_found, capitals)) # REMOVE CITIES THAT ARE NOT SUPPORTED BY THE API
        return capitals
    
    def __init__(self, city_name, temperature, feels_like_temperature, pressure, humidity, wind_speed):
        self.city_name = city_name
        self.temperature = self.kelvin_to_celsius(temperature)
        self.feels_like_temperature = self.kelvin_to_celsius(feels_like_temperature)
        self.pressure = pressure
        self.humidity = humidity
        self.wind_speed = wind_speed
        City.capital_objects.append(self)

    def kelvin_to_celsius(self, kelvin):
        celsius = round(kelvin, 2) - 273.15
        return round(celsius, 2)
    

class API:

    def __init__(self, api_key):
        self.api_key = api_key

    def create_city_with_weather_info(self, city_name):
        url = "https://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s" % (city_name, self.api_key)
        response = requests.get(url)
        data = response.json()
        temperature = data['main']['temp']
        feels_like = data['main']['feels_like']
        pressure = data['main']['pressure']
        humidity = data['main']['humidity']
        wind_speed = data['wind']['speed']
        City(city_name, temperature, feels_like, pressure, humidity, wind_speed)
