class Calculations:

    def get_condition(object: tuple):
        condition = object[1]
        return condition
    
    def create_list_of_pairs(attribute: str, city_list):
        return [(capital.city_name, getattr(capital, attribute)) for capital in city_list]

